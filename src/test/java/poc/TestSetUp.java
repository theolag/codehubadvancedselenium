package poc;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestSetUp {
  public static   WebDriver driver;
    @Before
    public void setup(){
        System.setProperty("webdriver.gecko.driver","C:\\workspace\\codehub\\drivers\\geckodriver.exe");
        driver = new FirefoxDriver();
    }
    @After
    public void teardown(){
        driver.quit();
    }
}
