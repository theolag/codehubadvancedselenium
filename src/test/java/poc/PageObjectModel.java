package poc;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageObjectModel {
    @FindBy(id="sidebar-user-login")
    private WebElement userNameField;
    @FindBy(id="sidebar-user-pass")
    private WebElement passWordField;
    @FindBy(id="sidebar-wp-submit")
    private WebElement submitButton;

    public PageObjectModel(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }
    public PageObjectModel fillUserNameField(String userName) {
        userNameField.sendKeys(userName);
        return this;
    }

    public PageObjectModel fillPasswordField(String passWord) {
        passWordField.sendKeys(passWord);
        return this;
    }

    public PageObjectModel submitForm() {
        submitButton.click();
        return this;
    }






}
