package poc;

import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static poc.UserActions.logIn;


public class TestExtraField extends TestSetUp {


    @Test
    public void verifyExtraField() {
     TestSetUp.driver.get("http://qasocial.vectordesign.gr/members/testdummy/profile/");
     String testField =  TestSetUp.driver.findElement(By.xpath("//*[@id=\"item-body\"]/div[2]/div/table/tbody/tr[2]/td[2]/p")).getText();
     assertEquals("Should Be Visible in Profile ONLy",testField);
    }

    @Test
    public void verifyExtraFriendsOnlyField() {
        TestSetUp.driver.get("http://qasocial.vectordesign.gr/members/panosam/profile/");
        String testField =  TestSetUp.driver.findElement(By.xpath("//*[@id=\"item-body\"]/div[2]/div/table/tbody/tr[2]/td[2]/p")).getText();
        assertEquals("Should Be Visible in Profile ONLy",testField);
    }

   @Test
    public void getAttExample(){
       TestSetUp.driver.get("http://qasocial.vectordesign.gr/");
System.out.println(TestSetUp.driver.findElement(By.id("wp-admin-bar-root-default")));
    }
}
