package poc;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.assertEquals;

public class SeleniumPoc {
    WebDriver driver;
    @Before
    public void setup(){
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
    }
@After
public void teardown(){
    driver.quit();
}
    @Test
    public void googleTitleTest() {
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();

         driver.get("https://www.google.com/");
        String pageTitle = driver.getTitle();
        assertEquals("google", pageTitle);

        driver.quit();
    }

    @Test
    public void inTitleTest() {
        driver.get("https://www.in.gr/");
        String pageTitle = driver.getTitle();
        assertEquals("in.gr | Όλες οι ειδήσεις - Ολοκληρωμένη κάλυψη ειδήσεων", pageTitle);
    }
}