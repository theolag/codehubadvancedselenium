package poc;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;


public class UserActions {
    
    public static void logIn(String userName, String passWord) {
        PageObjectModel pageObjectModel = new PageObjectModel(TestSetUp.driver);
        TestSetUp.driver.get("http://qasocial.vectordesign.gr/");
        pageObjectModel.fillUserNameField(userName);
        pageObjectModel.fillPasswordField(passWord);
        pageObjectModel.submitForm();
        WebDriverWait wait = new WebDriverWait(TestSetUp.driver,1);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("wp-admin-bar-my-account")));
        assertEquals("Howdy, Test Dummy",TestSetUp.driver.findElement(By.id("wp-admin-bar-my-account")).getText());
    }
}
