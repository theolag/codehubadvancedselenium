package helpers;



import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ScreenShotTools {


    /**
     * Compares to images for differences and marks them in a new picture file
     *
     * @param image1      String the accepted ui screenshot image path
     * @param image2      String image path to compare
     * @param outputImage String image path  for result output
     */

    public static void compareImages(String image1, String image2, String outputImage) {
        BufferedImage img1 = null;
        try {
            img1 = ImageIO.read(new File(image1));
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedImage img2 = null;
        try {
            img2 = ImageIO.read(new File(image2));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // retrieve image
            BufferedImage outImg = getDifferenceImage(img1, img2);
            File outputfile = new File(outputImage);
            ImageIO.write(outImg, "png", outputfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param img1 Buffered image (the accepted ui screenshot)
     * @param img2 Buffered image (the current ui screenshot)
     * @return outImg Buffered Image of differences
     */
    private static BufferedImage getDifferenceImage(BufferedImage img1, BufferedImage img2) {
        int width1 = img1.getWidth(); // Change - getWidth() and getHeight() for BufferedImage
        int width2 = img2.getWidth(); // take no arguments
        int height1 = img1.getHeight();
        int height2 = img2.getHeight();
        if ((width1 != width2) || (height1 != height2)) {
            System.out.println(width1+width2);
            System.exit(1);
        }
        // NEW - Create output Buffered image of type RGB
        BufferedImage outImg = new BufferedImage(width1, height1, BufferedImage.TYPE_INT_RGB);
        // Modified - Changed to int as pixels are ints
        int diff;
        int result; // Stores output pixel
        for (int i = 0; i < height1; i++) {
            for (int j = 0; j < width1; j++) {
                int rgb1 = img1.getRGB(j, i);
                int rgb2 = img2.getRGB(j, i);
                int r1 = (rgb1 >> 24) & 0xff;
                int g1 = (rgb1 >> 8) & 0xff;
                int b1 = (rgb1) & 0xff;
                int r2 = (rgb2 >> 24) & 0xff;
                int g2 = (rgb2 >> 8) & 0xff;
                int b2 = (rgb2) & 0xff;
                diff = Math.abs(r1 - r2);
                diff += Math.abs(g1 - g2);
                diff += Math.abs(b1 - b2);
                diff /= 3;
                // Change - Ensure result is between 0 - 255
                // The RGB components are all the same
                result = (diff << 8) | (diff << 24) | diff;
                outImg.setRGB(j, i, result); // Set result
            }
        }
        // Now return
        return outImg;
    }
}
